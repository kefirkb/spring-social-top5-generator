import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {AppComponent} from "./app.component";
import {ButtonModule} from "primeng/primeng";
import {WebSocketService} from "./services/websocket.service";
import {FacebookService} from "ng2-facebook-sdk";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ButtonModule
  ],
  providers: [WebSocketService, FacebookService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
