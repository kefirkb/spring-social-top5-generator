import {Component} from "@angular/core";
import {WebSocketService} from "./services/websocket.service";
import {FacebookInitParams, FacebookService, FacebookLoginResponse} from "ng2-facebook-sdk";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  fbParams: FacebookInitParams = {
    appId: '1044768165624130',
    xfbml: true,
    version: 'v2.5'
  };
  accessToken: string;

  public constructor(private webSocketService: WebSocketService,
                     private fb: FacebookService) {
    this.fb.init(this.fbParams);
  }

  public getData() {
    this.loginFacebook();
  }

  public loginFacebook() {
    this.webSocketService.data = '';
    this.fb.login().then(
      (response: FacebookLoginResponse) => {
        this.getFacebookPosts(response.authResponse.accessToken)
      },
      (error: any) => console.error(error)
    );
  }

  getFacebookPosts(token: string) {
    this.webSocketService.createData('ws://localhost:9000/hello', token);
  }

}
