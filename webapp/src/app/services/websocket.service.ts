import {Injectable} from "@angular/core";


@Injectable()
export class WebSocketService {

  private webSocket:WebSocket;

  public data: any;

  public createData(url:string, message:string) {

    this.webSocket = new WebSocket(url);

    this.webSocket.onmessage = (m:MessageEvent)=> {
      this.data = m.data;
      this.close();
    };
    this.webSocket.onopen = ()=> {
      this.webSocket.send(message);
    };

  }

  private close() {
    this.webSocket.close();
  }


}
