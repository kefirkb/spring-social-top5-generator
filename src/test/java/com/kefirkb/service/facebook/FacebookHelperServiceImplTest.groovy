package com.kefirkb.service.facebook

import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.runners.MockitoJUnitRunner

import static java.util.Arrays.asList
import static junit.framework.TestCase.assertEquals

/**
 * Created by Sergey on 08.02.2017.
 */
@RunWith(MockitoJUnitRunner.class)
class FacebookHelperServiceImplTest {

    @InjectMocks
    FacebookHelperServiceImpl facebookHelperService;

    @Test
    void testGetTagsFromStrings() {
        List<String> str = facebookHelperService.getTagsFromString("aaa #tag1 #tag4 #tag5 tag7 sdf #11")
        assertEquals(str.get(0), "#tag1")
        assertEquals(str.get(3), "#11")
        assertEquals(str.size(), 4)
    }

    @Test
    void testGetTagsFromString() {
        List<String> str = facebookHelperService.getTagsFromStrings(buildListSourceStrings());
        assertEquals(str.size(), 6);
    }

    @Test
    void testGetTopTags() {
        List<String> str = facebookHelperService.getTopTagsList(buildTags())
    }

    private List<String> buildListSourceStrings() {
        return asList("str1: #tag1 #tag2 #tag1", "str2:#tag4 #tag1 #tag6");
    }

    private List<String> buildTags() {
        return asList("#tag3", "#tag2", "#tag1", "#tag5", "#tag5", "#tag6", "#tag3", "#tag3");
    }
}
