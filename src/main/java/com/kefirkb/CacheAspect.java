package com.kefirkb;

import groovy.util.logging.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.cache2k.Cache;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.cache2k.CacheBuilder.newCache;

@Component
@Aspect
@Slf4j
public class CacheAspect {

    @Value(value = "${cache.duration}")
    private Integer cacheDurationSeconds;

    private Cache<String, List> tagsCache;
    private Cache<Integer, String> imageUrlCache;

    @PostConstruct
    public void initCache() {
        tagsCache = newCache(String.class, List.class)
                .expiryDuration(cacheDurationSeconds, SECONDS)
                .build();
        imageUrlCache = newCache(Integer.class, String.class)
                .expiryDuration(cacheDurationSeconds, SECONDS)
                .build();
    }

    @Around(value = "execution(* com.kefirkb.service.facebook.FacebookHelperService.getTopTags(..)) " +
            "&& args(accessToken)")
    public List cacheUserRight(ProceedingJoinPoint jp, String accessToken) throws Throwable {

        if (tagsCache.contains(accessToken)) {
            return tagsCache.get(accessToken);
        }
        List result = (List) jp.proceed();
        tagsCache.put(accessToken, result);
        return result;
    }

    @Around(value = "execution(* com.kefirkb.service.image.ImageGeneratorService.getUrlGeneratedImage(..))" +
            "&& args(listTags)")
    public String cacheImageUrl(ProceedingJoinPoint jp, List<String> listTags) throws Throwable {
        Integer hash = listTags.hashCode();

        if (imageUrlCache.contains(hash)) {
            return imageUrlCache.get(hash);
        }
        String result = (String) jp.proceed();
        imageUrlCache.put(hash, result);
        return result;
    }

    @PreDestroy
    public void destroy() {
        tagsCache.clear();
    }
}
