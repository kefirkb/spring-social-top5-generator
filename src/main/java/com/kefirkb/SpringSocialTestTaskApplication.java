package com.kefirkb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.social.config.annotation.EnableSocial;

import java.io.IOException;

@SpringBootApplication
@EnableAutoConfiguration
@EnableConfigurationProperties
@EnableSocial
public class SpringSocialTestTaskApplication {

    public static void main(String[] args) throws IOException {
        SpringApplication.run(SpringSocialTestTaskApplication.class, args);
    }
}
