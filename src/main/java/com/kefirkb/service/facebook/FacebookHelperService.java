package com.kefirkb.service.facebook;

import java.util.List;

/**
 * Created by Sergey on 07.02.2017.
 */
public interface FacebookHelperService {
    List<String> getTopTags(String accessToken);
}
