package com.kefirkb.service.facebook;

import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.Post;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Objects.nonNull;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.*;

/**
 * Created by Sergey on 07.02.2017.
 */
@Service
public class FacebookHelperServiceImpl implements FacebookHelperService {

    private Pattern tagMatcher = Pattern.compile("[#]+[A-Za-z0-9-_]+\\b");


    @Override
    public List<String> getTopTags(String accessToken) {

        Facebook facebook = new FacebookTemplate(accessToken);
        PagedList<Post> feed = facebook.feedOperations().getFeed();

        List<String> messages = feed
                .stream()
                .filter(f -> nonNull(f.getMessage()))
                .map(Post::getMessage)
                .collect(toList());

        return getTopTagsList(getTagsFromStrings(messages));
    }

    public List<String> getTagsFromStrings(List<String> list) {

        List<String> allTags = new ArrayList<>();
        return list.stream().filter(s -> s.contains("#"))
                .flatMap(f -> getTagsFromString(f).stream())
                .collect(toList());
    }

    List<String> getTagsFromString(String str) {
        Matcher m = tagMatcher.matcher(str);
        List<String> l = new ArrayList<>();

        while (m.find()) {
            String s = m.group();
            l.add(s);
        }
        return l;
    }

    List<String> getTopTagsList(List<String> list) {
        return list
                .stream()
                .collect(groupingBy(identity(), counting()))
                .entrySet()
                .stream()
                .sorted((e1, e2) -> e2.getValue().compareTo(e1.getValue()))
                .map(Map.Entry::getKey)
                .limit(5)
                .collect(toList());
    }
}
