package com.kefirkb.service.image;

import com.kennycason.kumo.CollisionMode;
import com.kennycason.kumo.WordCloud;
import com.kennycason.kumo.WordFrequency;
import com.kennycason.kumo.bg.RectangleBackground;
import com.kennycason.kumo.font.scale.LinearFontScalar;
import com.kennycason.kumo.image.AngleGenerator;
import com.kennycason.kumo.nlp.FrequencyAnalyzer;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Sergey on 07.02.2017.
 */
@Service
@NoArgsConstructor
public class ImageGeneratorServiceImpl implements ImageGeneratorService {
    @Override
    public String getUrlGeneratedImage(List<String> strings, String imageName) throws IOException {

        final FrequencyAnalyzer frequencyAnalyzer = new FrequencyAnalyzer();
        final List<WordFrequency> wordFrequencies = frequencyAnalyzer.load(strings);
        final Dimension dimension = new Dimension(600, 600);
        final WordCloud wordCloud = new WordCloud(dimension, CollisionMode.RECTANGLE);
        wordCloud.setPadding(0);
        wordCloud.setBackground(new RectangleBackground(dimension));
        wordCloud.setFontScalar(new LinearFontScalar(10, 40));
        wordCloud.setAngleGenerator(new AngleGenerator(0, 0, 10));
        wordCloud.build(wordFrequencies);
        String url = new File("./").getCanonicalPath() + File.separator + "img" + File.separator + imageName + ".png";
        wordCloud.writeToFile(url);
        return "/img/" + imageName + ".png";
    }
}
