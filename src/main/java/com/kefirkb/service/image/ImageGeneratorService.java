package com.kefirkb.service.image;

import java.io.IOException;
import java.util.List;

/**
 * Created by Sergey on 07.02.2017.
 */
public interface ImageGeneratorService {

    String getUrlGeneratedImage(List<String> strings, String imageName) throws IOException;
}
