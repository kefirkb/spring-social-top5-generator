package com.kefirkb.service;

import com.kefirkb.service.facebook.FacebookHelperService;
import com.kefirkb.service.image.ImageGeneratorService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.util.List;

/**
 * Created by sergey on 06.02.17.
 */

@Slf4j
@Component
@NoArgsConstructor
@AllArgsConstructor
public class CustomHandler extends TextWebSocketHandler {

    @Autowired
    private ImageGeneratorService imageGeneratorService;
    @Autowired
    private FacebookHelperService facebookHelperService;

    private WebSocketSession session;

    @Override
    public void afterConnectionEstablished(WebSocketSession session) {
        //log.info("Connection established");
        this.session = session;
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        //log.info(message.getPayload());
        String accessToken = message.getPayload();
        List<String> result = facebookHelperService.getTopTags(accessToken);
        String url = imageGeneratorService.getUrlGeneratedImage(result, accessToken);
        this.session.sendMessage(new TextMessage(url));
    }

}
